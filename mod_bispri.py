#!usr/bin/env python

"""Modulo con dos funciones. La primera funcion recibe un numero y devuelve True o False si este numero es primo o no.
La segunda funcion recibe un year en un rango dado de 1900 a 3000 y devuelve si el year es bisiesto o no (True o False).
Este modulo esta hecho para ser utilizado por los programas prueba_primo.py y prueba_bisiesto.py"""


def es_primo(x):
	"""La funcion recibe un numero y devuelve True si dicho numero es
           primo o False si no lo es."""
	#El algoritmo es sencillo, busco si el numero es divisible por mas de 		un numero (sin tomar en cuenta el uno) en cuyo caso debe ser compuesto. 
	valor = bool
	i=0
	numbers = [2,3,4,5,6,7,8,9]
	for p in numbers:
		if x%p == 0:
			i += 1
	
	if i > 1:
		valor = False
	else:
		valor = True
	return valor


#El algoritmo para years bisiestos es un poco mas complicado. Un year es bisiesto si es divisible entre 4 excepto si tambien es divisible entre 100, entonces no es bisiesto pero si es divisible entre 4,100 y 400 entonces si es bisiesto.

def es_bisiesto(x):
	"""La funcion recibe un year en el rango de 1900 a 3000 y devuelve True 	   si este es bisiesto, caso contrario regresa False"""
	#Defino una variable para la ejecucion continua del programa y defino 		el rango de years permitido.
	valor = bool
	if x<1900 or x>3000:
		print "Error, los lustros deben estar en el rango de 1900 a 3000"
		return False
	else:
		#Ahora defino las condiciones para que el year sea bisiesto.	
		if x%4 == 0:

			if x%100 == 0 and x%400 != 0:
				valor = False

                	elif x%100 == 0 and x%400 == 0:
				valor = True
			else:
				valor = True
		else:
			valor = False

		return valor
