#!/usr/bin/env python


"""Este programa escribe datos numericos en un archivo de texto. El programa genera numeros en un rango dado y luego los escribe en dicho archivo.
Recibe tres parametros en tiempo de ejecucion: La ruta del archivo a escribir, el modo ('w' o 'a') y el limite superior del rango de numeros. Si solo se especifica el nombre del archivo el programa lo escribe en el directorio actual"""



print "Escriba la ruta del archivo"
try:
	#El programa recibe los parametros en tiempo de ejecucion
	ruta = raw_input()
	#El metodo lower de los string convierte a minuscula los datos recibidos
	print "en modo a o w?"
	modo = raw_input().lower()
	#Defino una variable para que el programa no se interrumpa
	#violentamente en caso de que el usuario no ingrese correctamente el 		modo
	continuacion = True
	if modo != "a" and modo != "w":
		print "Debe digitar a o w solamente"
		continuacion = False
		
	else:
		print "Los numeros se generan de cero a?"

		limite = int(raw_input())

		

	if continuacion:
	#El algotitmo escribe el archivo cada 10 numeros
		lista = range(0,limite+1)
		linea = ""
		puntero = open(ruta, modo)
		for x in lista:
			linea += ",%d"%(x)
			if x%10 == 0 and x!=0:
				puntero.write(linea[1:])
				puntero.write('\n')
				linea = ""

		puntero.close()
		print "archivo escrito!"



	else:
		print "Debido a los errores debe de volver a llamar al programa"

#Valido que el usuario digite solo enteros y que la ruta del archivo sea bien ingresada			
except ValueError:
		print "Error, debe de escribir un numero entero"
except IOError:
		print "El archivo no existe en la ruta especificada o la ruta fue mal ingresada"
	
