#!/usr/bin/env python

"""Modulo con funciones matematicas, una para aproximar el valor de la raiz cuadrada de un numero utilizando el metodo de Newton y otra para calcular el valor de la constante pi utilizando la formula de wallis, tambien incluye una funcion para obtener los terminos de la serie de Fibonacci.
Este modulo esta hecho para ser utilizado por los programas prueba_raiz2.py y prueba_pi.py """


def calculo_pi(N):
	"""Funcion para aproximar pi mediante la formula de wallis.
	El valor N que recibe es el numero maximo de multiplicaciones (la 		formula de wallis se define con un multiplicador)."""

	n = 1
	pi = 1.0
	#Esta condicion no impide que el programa reciba valores no enteros 		pero la excepcion se manejara en el programa prueba_pi.py que importara 	esta funcion.
	while n <= N:
		 
		p=(4*n**2)
		s=(2*n - 1) * (2*n + 1)
		pi *= float(p)/s
		n += 1

	return 2*pi
	#La formula de wallis calcula pi/2 es por esto que el resultado se 		multiplica por 2 para obtener pi.


def raiz2_newton(x, err):
	"""Funcion para aproximar la raiz cuadrada de un numero. 
	La funcion recibe dos parametros: el numero al que se le desea
        aproximar su raiz cuadrada y el numero maximo de iteraciones""" 
	y = x*0.1
	b = err + 1.0
	while b > err:
		y1 = y
		y = x/(2*y1) + y1/2
 		y2 = y
		b = abs(y2 - y1)
	return y 

def fibonacci(n):

	"""Calculo de la serie de fibonacci.
	La funcion recibe el termino que se desea calcular y devuelve el 		resultado"""
	n_0 = 1
	n_1 = 1
	if n<0:
		print "n debe de ser positivo"
		
	#El manejo de excepciones se realiza en 
	#el programa que importe la funcion.
	elif n == 0 or n == 1:
		return 1

	else:
		while n > 1:
			y = n_0 + n_1
			n -= 1
			n_0 = n_1
			n_1 = y
		return y
		

		
