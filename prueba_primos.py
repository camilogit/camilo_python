#!/usr/bin/env python

"""Este programa recibe una cantidad variable de numeros , imprime cuales de ellos son primos y devuelve la cantidad de numeros primos de los parametros recibidos"""


try:
	from mod_bispri import es_primo, es_bisiesto

	#Defino una funcion que reciba una cantidad variable de parametros, 		devuelve la cantidad de ellos que sea primo y los imprime.
	def revisar_primos(*varios):
		i = 0
		for p in varios:
			if es_primo(p) == True:
				print "%d es primo"%(p)
				i += 1
		
		
		return i

	#Defino una lista para colocar los numeros digitados por el usuario y 		defino la condicion para dejar de ingresarlos. En este caso la funcion 		isdigit valida que se ingrese valores numericos enteros y no 
        #es necesario usar el ValueError.

	lista = []
	print "Digite los numeros a revisar, cuando termine digite stop"
	while True:
		palabra = raw_input().lower()
		if palabra == "stop":
			break
	
		elif palabra.isdigit() == False:
			print "debe de escribir solo numeros enteros. Para salir digite stop"
		else:
			palabra = int(palabra)
			lista.append(palabra)

	#Con el for itero sobre la lista aplicando la funcion que importe del 		modulo.
	for elemento in lista:
		hache = revisar_primos(elemento)

	#Valido que el modulo mod_bispri este presente.	
except ImportError:
	print "Error, el modulo mod_bispri.py debe de estar en el mismo directorio en donde esta este programa para poder ser ejecutado"
	exit()

