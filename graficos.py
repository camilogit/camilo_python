#!/usr/bin/env python

"""Este programa realiza dos graficos de polinomios y sus derivadas.
El 1er grafico se realiza en un grid 1x2, en el de la izquierda se grafica el polinomio y en el de la derecha su derivada.
El 2do grafico se realiza en un grid 1x1 y corresponde a el polinomio y las rectas tangentes al mismo en los puntos especificados"""



#Unas indicaciones que se imprimiran en la consola.
print "Los graficos aparecen en ventanas individuales." 
print "Para poder utilizar la consola debe de cerrar todas" 
print "las ventanas de los graficos"



try:
	import numpy
	import matplotlib.pyplot as matpy
	#Valido la presencia de las librerias cientificas


	matpy.figure("Grafico de un polinomio y su derivada")

	#Defino dos polinomios con numpy
	f = numpy.poly1d([6,9,0,-2])
	f_prima = f.deriv()
	x = numpy.linspace(-2, 2, 40)

	#Defino la division de la pantalla en dos, una posicion para cada uno.
	matpy.subplot(1,2,1)
	matpy.plot(x, f(x), 'r')
	matpy.title("$f(x)=6x^{3}+9x^{2}-2$")

	matpy.subplot(1,2,2)
	matpy.plot(x, f_prima(x), 'b')
	matpy.title(r"$\frac{df}{dx}=18x^{2}+18x$")


	#Para el segundo grafico defino un espacio para la funcion y defino 		el polinomio y su derivada.
	x2 = numpy.linspace(-3, 3, 1000)
	f2 = numpy.poly1d([2,5,0,-2])
	f2_pr = f2.deriv()
	matpy.figure("Grafico de un polinomio y su derivada parte2")


	#Genero una funcion para calcular el valor del intercepto de la recta 		tangente
	def incpt(x,y):
		b = y-f2_pr(x)*x
		return b

	matpy.plot(x2, f2(x2), 'g')

	
	#Las rectas tangentes en los punto dados son creadas a continuacion:
	x3 = numpy.array([-2.8, -2, -1.2])
	matpy.plot(x3, f2_pr(-2)*x3 + incpt(-2,f2(-2)), 'b', -2, f2(-2), 'y^' )

	x3 = numpy.array([-1, -0.5, 0])
	matpy.plot(x3, f2_pr(-0.5)*x3 + incpt(-0.5,f2(-0.5)), 'b', -0.5,f2(-0.5), 'y^')

	x3 = numpy.array([1, 1.5, 1.9])
	matpy.plot(x3, f2_pr(1.5)*x3 + incpt(1.5,f2(1.5)), 'b', 1.5,f2(1.5), 'y^')

	matpy.show()
except ImportError:
	print "Las librerias cientificas numpy y matplotlib deben de estar"
	print "instaladas en el sistema para correr el programa."




