#!/usr/bin/env python
"""Este programa recibe dos parametros por linea de comando una ruta de un archivo y un entero N. El programa escribe de 10 en 10 en un archivo de texto una lista de numeros de orden N en la ruta que se especifico. Si el archivo no existe a la hora de llamar al programa este lo crea, si el archivo ya existe lo sobreescribe. Este programa fue creado para trabajar en conjunto con el programa procesador_numeros.py"""

try:
	import sys
	import random

	#Primero valido que el usuario pase la cantidad correcta de parametros
	if len(sys.argv) != 3:
		print "los parametros deben de pasarse por linea de comando."  
        	print "ej:usuario@maquina:~$ python generador_numeros.py ./texto.txt 150"
	else:

	

		#Creo las variables necesarias para llevar a cabo el algoritmo
		N = int(sys.argv[2])
		i=0
		lista=[]
		linea = ""

		#Genero la lista de numeros aleatorios en un rango de 0 a 1000. 		Debido a que el contador inicia en cero la condicion es i<N de 			esta manera la lista tiene el orden deseado por el usuario.
		while i < N:

			lista.append(random.randint(0,1000))
			i += 1

		puntero = open(sys.argv[1],'w')
		#El puntero en modo 'w' crea el archivo si este no existe, caso 		contrario lo sobreescribe. 
		#En mi algoritmo defino dos contadores para accesar los 		elementos de la lista por rango segun sea N<10 o no.
		s=0
		f=9
		k = N%10
		
		if N>=10:
		#la condicion f<N-k asegura que no exista IndexError al momento 		de sumar 10 unidades al contador f
			while f<N-k:
				for p in lista[s:f+1]:
					linea += ",%d"%(p)
					
						
				puntero.write(linea[1:])
				puntero.write('\n')
				linea = ""		
				f += 10
				s += 10
			
			puntero.close()
			#Si N%10 no es cero y N > 10 el algoritmo anterior 				escribe los numeros excepto un residuo N-K para agregar 			esta parte defino un nuevo puntero y lo utilizo en modo 			'a' de manera que las lineas se agregan al final del 				archivo anterior y no se sobrescribe.
			
			puntero2 = open(sys.argv[1],'a')
			for p in lista[f-9:N]:
				linea += ",%d"%(p)
			puntero2.write(linea[1:])
			puntero2.write('\n')	
        		puntero2.close()
			#En el caso de que N%10 = 0 el algoritmo anterior con 				puntero2 no presenta problemas ya que  lista[f:N-1] = 				[] (vacia) y no se agrega nada al final.        
		else:
		
			for p in lista:
				linea += ",%d"%(p)
			puntero.write(linea[1:])
			puntero.write('\n')
			linea = ""
			puntero.close()

		print "Archivo escrito!"
#Valido que los parametros se pasen correctamente y que los modulos sys y random esten instalados.
except ValueError:

		print "El primer parametro debe de ser la ruta de un archivo y"
		print "el segundo un numero entero. Se pasan por linea de comando"
		print " ej:usuario@maquina:~$ python generador_numeros.py ./texto.txt 150"
	
except ImportError:

		print "Los modulos sys y random son necesarios para correr el programa"

exit()	
	

