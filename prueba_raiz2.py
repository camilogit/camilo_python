#!usr/bin/env python

"""Este programa hace uso de la funcion raiz2_newton.py del modulo mod_calculador.py para aproximar el valor de la raiz cuadrada de un numero entero mediante el metodo de Newton y compara dicho valor con el obtenido utilizando la funcion math.sqrt de la libreria math"""

try:
	import math
	from mod_calculador import raiz2_newton

	#Utilizo raw_input para recibir los parametros en tiempo de ejecucion.

	print "Raiz cuadrada de que numero desea calcular?"
	x = int(raw_input())
	print "Cual es la tolerancia maxima?"
	y = float(raw_input())
	z = math.sqrt(x)
	resultado = raiz2_newton(x, y)

	print "La raiz de %d utilizando el metodo de newton es %f"%(x,resultado)
	print "La tolerancia fue %f"%(y)
	print "El valor de la funcion math.sqrt de la libreria math es %f"%(z)

	#Valido que el usuario ingrese solo numeros enteros y que el modulo y 		las librerias esten presentes para la ejecucion del programa.
except ValueError:

	print "Debe de escribir solo enteros"

except ImportError:

	print "El modulo mod_calculador.py debe de estar en el mismo directorio en donde "
	print "se encuentra este programa para poder ser ejecutado, ademas se requiere de"
	print "la libreria de python math."
	
