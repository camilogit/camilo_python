#!/usr/bin/env python

"""Funcion que recibe la ruta de un archivo de texto, lo lee y devuelve una lista con el contenido de cada linea habiendo eliminado el caracter de cambio de linea /n. La lista es imprimida en la pantalla. La ruta se pasa por linea de comando """

try:
	import sys

	#1ero valido que la cantidad de parametros es la correcta
	if len(sys.argv) != 2:
		print "Error, debe de escribir solo la ruta del archivo por linea de comando, es decir al llamar el programa." 
		print "Ej:  usuario@maquina:~$ python prueba_bisiesto.py ./texto.txt" 
	else:
		try:

			#El algoritmo hace uso del metodo de los string strip 				para eliminar el caracter de cambio de linea.
			#el metodo readlines devuelve la lista con las lineas 				del archivo.	
			x = sys.argv[1]

			puntero = open(x, 'r')

			lista_lineas = puntero.readlines()
			lista_lineas2 = []

			for p in lista_lineas:
				lista_lineas2.append(p.strip('\n'))

			print lista_lineas2

		#Valido que el archivo exista en la ruta especificada
		except IOError:

			print "Error, archivo inexistente"
			exit()

#Valido que el modulo para pasar los parametros este presente
except ImportError:

	print "El modulo sys debe de estar presenta para que funcione el programa"
