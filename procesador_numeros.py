#!/bin/usr/env python

"""Este programa esta hecho para procesar el archivo creado por generador_numeros.py, el programa recibe la ruta de un archivo por linea de comando e indica la cantidad de numeros en el archivo, el valor de la suma de dichos numeros y su promedio"""

try:
	import sys
	#Valido la cantidad de parametros en la llamada
	if len(sys.argv) != 2:

		print "Error!, debe de ingresar solo la ruta del archivo y debe de hacerlo por linea de comando."
		print "Ej:usuario@maquina:~$ python procesador_numeros.py ./texto.txt"
	
	else:
	
		try:
			#Defino el puntero en la ruta especificada asi como los 			contadores para sacar la suma de los numeros y el 				promedio.		
			ptr = open(sys.argv[1],'r')
			i = 0
			suma = 0
			promedio = 0.0
			num = 0

			#Escribo el archivo
			while True:
				
				linea = ptr.readline()
				#Elimino el caracter de cambio de linea para 					poder convertir las entradas a integer
				linea = linea.strip('\n')
				#Con la funcion split consigo crear listas con 					los numeros de cada linea. El contador i me da 					la cantidad de numeros.			
				lista = linea.split(",")

			
				for x in lista:
					#La funcion is.digit permite validar 						que las entradas sean numericas, este 						paso lo agregue debido a que el paso 						anterior crea una ultima lista con una 						sola entrada de tipo string lo que 						impedia la conversion de las entradas a 					tipo float utilizando el for.
					if x.isdigit() == True:
						suma += float(x)
						i += 1
					else:
						num += 1
				if not linea:
					break
			ptr.close()
			try:
				promedio = suma/i
			except ZeroDivisionError:


			
				print "No hay numeros en el archivo. Para que el programa funcione optimamente el archivo debe de contener solo numeros separado por comas. Este programa se hizo especificamente para procesar el archivo creador por generador_numeros.py"

			#Ahora considero diferentes situaciones en la ejecucion 			del programa y las respuestas del mismo ante estas 				situaciones.

			if num > 1 and i != 0:
				print "Hay %d numeros en el archivo, su suma da %d y su promedio es %f"%(i, suma, promedio)
				print "Nota: El archivo  %s contiene caracteres no numericos. Este programa se hizo especificamente para procesar el archivo creador por generador_numeros.py y de esa manera debe de utilizarse para tener resultados optimos"%(sys.argv[1])
		
			elif i != 0:
				print "Hay %d numeros en el archivo, su suma da %d y su promedio es %f"%(i, suma, promedio)				 
				
		#Valido que el archivo exista en la ruta especificada		
		except IOError:

			print "Error al ingresar la ruta. Archivo inexistente"
		
			
#Valido que el modulo sys este presente. Las otras excepciones fueron aplicadas de manera anidada.
except ImportError:

	print "Se necesita el modulo sys para poder correr este programa"			
