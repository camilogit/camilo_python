#!/usr/bin/env python

"""Este programa recibe un rango de years por linea de comando, busca cuantos y cuales years son bisiestos en ese rango. Imprime los years bisiesto en la pantalla"""

try:
	import sys
	from mod_bispri import es_bisiesto

	#Valido la cantidad de parametros y defino una lista vacia para poner 		los years que sean bisiestos.
	lista_lustros = []
	if len(sys.argv) != 3:
		print "Error!, Debe de pasar dos parametros por linea de comando, es decir al llamar al programa en la consola." 
		print "Ej:  usuario@maquina:~$ python prueba_bisiesto.py 2003 2156"

	#Valido el rango permitido para los years.
	elif int(sys.argv[1]) < 1900 or 3000 < int(sys.argv[2]):
		print "El rango de years debe de estar entre 1900 y 3000"
	#Creo una lista con los numeros en el rango dado por los parametros 		pasados por el usuario. Utilizo un for para iterar sobre las entrada de 	dicha lista usando la funcion es_bisiesto.
	else:	
		lista2 = range(int(sys.argv[1]),int(sys.argv[2])+1)
		for lustro in lista2:
				
			if es_bisiesto(lustro) == True:
				lista_lustros.append(lustro)
		print "En el rango dado hay %d years bisiestos y son:"%(len(lista_lustros))
		print lista_lustros

#Valido que se ingrese correctamente los parametros y que el modulo mod_bispri.py este presente.		
except ImportError:

	print "Error, el modulo mod_bispri.py debe de estar en el mismo directorio en donde esta este programa para poder ser ejecutado, ademas el modulo sys es requerido"

except ValueError:

	print "Debe de ingresar solo numeros enteros"	
exit()		
