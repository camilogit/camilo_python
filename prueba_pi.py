#!/usr/bin/env python

"""Este programa recibe un entero por linea de comandos que sirve como limite maximo de multiplicaciones para aproximar el valor de la constante pi mediante la formula de wallis"""

try:
	import math
	import sys
	from mod_calculador import calculo_pi
	# Valido la cantidad de parametros
	if len(sys.argv) != 2:

		print "Error!, debe de pasar un parametro por linea de comando"
		print "Ej:usuario@maquina:~$ python prueba_pi.py 2000"
	#Valido que la entrada sea un entero
	elif sys.argv[1].isdigit() == False:
		print "Debe de ingresar solo numeros enteros"
	else:
	#Defino las variables necesaria para comparar mi resultado con el de la 	funcion math.pi de la libreria math
		x = calculo_pi(int(sys.argv[1]))
		y = math.pi
		z = abs(y - x)
		print "El valor obtenido por mi programa mediante la formula de wallis con N=%s"%(sys.argv[1])
		print "es %f"%(x)
		print "El valor de la funcion math.pi de la libreria de python math es %f"%(y)
		print "Al compararlos la diferencia es %f"%(z)
		
except ImportError:
#Imprimo el mensaje de esta forma para que no se corte de forma fea en la pantalla.

		print "El modulo mod_calculador.py debe de estar en el mismo"
		print " directorio en donde se encuentra este programa para"
		print " poder ser ejecutado, ademas se requiere de las"
		print " librerias de python sys y math"

